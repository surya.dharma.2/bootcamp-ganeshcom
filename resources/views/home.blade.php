<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/home_style.css">
    <title>Home | Guna Guide</title>
</head>
<body>
    <div class="format">
        <div class="img1">
            <img src="img/tes2.jpg">
            <h2>VISIT</h2>
            <h1>BALI</h1>
            <p>
                Guna Guide merupakan website perjalanan wisata di Bali dimana <br/>
                kami akan merekomendasikan seluruh perjalanan kamu berdasarkan <br/>
                budget, durasi dan jumlah orang yang kamu ajak.
             </p>
        </div>
        <div class="img2">
            <h1>
                Mulai Sekarang ! <br/>
                GRATIS
            </h1>
        </div>
        <div class="img3">
            <h2>Dengan lebih dari</h2>
            <h1>
                10.000+<br/><br/>
                3.000+<br/><br/>
                1.000+
            </h1>
            <div class="hotel">
                <p>
                    Hotel & Resort
                </p>
            </div>
            <div class="resto">
                <p>
                    Restoran
                </p>
            </div>
            <div class="wisata">
                <p>
                    Tempat wisata
                </p>
            </div>
        </div>
        <div class="img4">
            <h2>Mudah digunakan</h2>
            <div class="pertama">
                <img src="img/Langkah1.jpeg">
                <h1>
                    Tentukan durasi liburan.
                </h1>
            </div>
            <div class="kedua">
                <img src="img/Langkah2.jpg">
                <h1>
                    Tentukan jumlah orang yang kamu ajak.
                </h1>
            </div>
            <div class="ketiga">
                <img src="img/langkah3.jpg">
                <h1>
                    Tentukan estimasi budget kamu.
                </h1>
            </div>
            <div class="keempat">
                <h1>
                    Pilih kategori liburan yang kamu mau.
                </h1>
            </div>
            <div class="kelima">
                <h1>
                    Selesai dan rekomendasi perjalanan akan muncul.
                </h1>
            </div>
        </div>
        <div class="img5">
            <h1>
                Jadi Gaperlu Bingung <br/>
                Rencanain Liburan <br/>
                kamu
            </h1>
        </div>
        <div class="footer">
            <div class="layanan">
                <h1>LAYANAN</h1>
                <p>
                    <a href="#">Saran Destinasi</a><br/>
                    <a href="#">Hubungi Kami</a>
                </p>
            </div>
            <div class="dukungan">
                <h1>DUKUNGAN</h1>
                <p>
                    <a href="#">Tentang</a><br/>
                    <a href="#">Ketentuan</a><br/>
                    <a href="#">Kebijakan Privasi</a>
                </p>
            </div>
            <div class="ikuti">
                <h1>IKUTI KAMI</h1>
                <p>
                    <a href="#">Instagram</a><br/>
                    <a href="#">Twitter</a><br/>
                    <a href="#">Facebook</a>
                </p>
            </div>
        </div>
    </div>
</body>
</html>